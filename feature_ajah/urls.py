from django.urls import path
from . import views

app_name = 'feature_ajah'
urlpatterns = [
    path('', views.movies, name="movies"),
    path('listForm/', views.listForm, name="listForm"),
    path('forms/', views.forms, name="forms"),
]