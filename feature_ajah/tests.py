from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import MoviesModel
from .views import listForm, forms, movies
from .forms import MoviesForm
from .apps import FeatureAjahConfig

# Create your tests here.

class TestModel(TestCase):
    def setUp(self):
        self.MoviesModel = MoviesModel.objects.create(
            movie_name="La la land", rating="7", review="bagus"
        )

    def test_instance_created(self):
        self.assertEqual(MoviesModel.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.MoviesModel), "La la land")


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.MoviesModel = MoviesModel.objects.create(
            movie_name="La la land", rating="7", review="bagus"
        )
        self.forms = reverse("movies:forms")
        self.listForm = reverse("movies:listForm")
        self.movies = reverse("movies:movies")

    def test_GET_movies(self):
        response = self.client.get(self.movies)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'movies.html')

    def test_GET_forms(self):
        response = self.client.get(self.forms)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'forms.html')

    def test_POST_forms(self):
        response = self.client.post(self.forms,{'movie_name': 'La la land', 'rating': "7", 'review':"bagus"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_forms_invalid(self):
        response = self.client.post(self.forms,{'movie_name': '', 'rating': "7", 'review':""}, follow = True)
        self.assertTemplateUsed(response, 'listForm.html')
    

    def test_GET_listForm(self):
        response = self.client.get(self.listForm)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listForm.html')

class TestUrls(TestCase):
    def test_forms_use_right_function(self):
        found = resolve('/movies/forms/')
        self.assertEqual(found.func, forms)

    def test_listForm_use_right_function(self):
        found = resolve('/movies/listForm/')
        self.assertEqual(found.func,listForm)

    def test_movies_use_right_function(self):
        found = resolve('/movies/')
        self.assertEqual(found.func,movies)


class TestForm(TestCase):
    def test_form_valid(self):
        form = MoviesForm(
            data={
                "movie_name": "La la land",
                "rating": "7",
                "review": "bagus"
            }
        )
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = MoviesForm(data={})
        self.assertFalse(form.is_valid())

    
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(FeatureAjahConfig.name, 'feature_ajah')
