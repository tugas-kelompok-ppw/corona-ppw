from django.contrib import admin

# Register your models here.
from .models import MoviesModel

admin.site.register(MoviesModel)