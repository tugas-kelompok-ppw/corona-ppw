from django.db import models

# Create your models here.

class MoviesModel(models.Model):
    movie_name     = models.CharField(max_length = 50)
    rating      = models.IntegerField()
    review       = models.CharField(max_length = 1000, null = True)

    published       = models.DateTimeField(auto_now_add = True)
    updated         = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.movie_name