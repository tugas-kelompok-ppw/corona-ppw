from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import MoviesForm
from .models import MoviesModel

def listForm(request):
    movies = MoviesModel.objects.all()
    context = {
        'movies':movies,
    }

    return render(request,'listForm.html',context)

def forms(request):
    movies_form = MoviesForm()

    if request.method == 'POST':
        MoviesModel.objects.create(
            movie_name       = request.POST.get('movie_name'),
            rating        = request.POST.get('rating'),
            review    = request.POST.get('review'),
        )
        return redirect('movies:listForm')

    context = {
        # 'page_title':'Add course',
        'movies_form':movies_form
    }

    return render(request,'forms.html',context)


def movies(request):
	return render(request, "movies.html")

