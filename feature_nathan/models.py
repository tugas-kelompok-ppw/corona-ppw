from django.db import models


class resep(models.Model):
    nama = models.CharField(max_length=120, null=True)
    bahan = models.CharField(max_length=700, null=True)
    alat = models.CharField(max_length=700, null=True)
    langkah = models.CharField(max_length=1500, null=True)


# Create your models here.
