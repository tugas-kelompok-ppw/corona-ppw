from django.urls import path
from . import views

app_name = 'feature_nathan'
urlpatterns = [
    path('', views.index, name="index"),
    path('isiform/', views.inputform, name='inputform'),
    path('mintaresep/', views.mintaresep, name='mintaresep'),

]
