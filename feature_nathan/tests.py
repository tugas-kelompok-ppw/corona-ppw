from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.test import client
from django.apps import apps
from .models import resep
from .views import index, inputform, mintaresep
from .forms import buatresep
from .apps import FeatureNathanConfig

class TestApp(TestCase):
    def test_app_is_exist(self):
        self.assertEqual(FeatureNathanConfig.name, 'feature_nathan')
        self.assertEqual(apps.get_app_config('feature_nathan').name, 'feature_nathan')

class TestIndex(TestCase):
    def test_url_bantuan_is_exist(self):
        response = Client().post('/health/')
        self.assertEqual(response.status_code, 200)

    def test_index_template(self):
        response = Client().get('/health/')
        self.assertTemplateUsed(response, 'health.html')


class TestIsiForm(TestCase):
    def test_url_bantuan_is_exist2(self):
        response = Client().post('/health/isiform/')
        self.assertEqual(response.status_code, 200)

    def test_isiform_template(self):
        response = Client().get('/health/isiform/')
        self.assertTemplateUsed(response, 'isidong.html')

class TestMintaResep(TestCase):
    def test_url_bantuan_is_exist3(self):
        response = Client().get('/health/mintaresep/')
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        mintaresep = resep(nama='nasgor')
        self.assertEqual(resep.objects.all().count(), 0)

    def test_mintaresep_template(self):
        response = Client().post('/health/mintaresep/',{'nama':'nasgor','bahan':'apa ya?','alat':'apa','langkah':'gofud'})
        self.assertEqual(response.status_code, 302)

    def test_model_can_create_resep(self):
        newQuestion = resep.objects.create(nama="nasgor", bahan="apa ya?", alat="apa", langkah ="gofud")
        counting_all_question = resep.objects.all().count()
        self.assertEqual(counting_all_question, 1)


