from django import forms
from .models import resep


class buatresep(forms.Form):
    nama = forms.CharField(
        label="Nama",
        max_length=120,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'required': True,
                'style': 'margin-bottom: 4%; margin-top: 2%;',
                'size': '40'
            }
        )
    )
    bahan = forms.CharField(
        label="bahan",
        max_length=700,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'required': True,
                'style': 'margin-bottom: 4%; margin-top: 2%;',
                'size': '40'
            }
        )
    )
    alat = forms.CharField(
        label="alat",
        max_length=700,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'required': True,
                'style': 'margin-bottom: 4%; margin-top: 2%;',
                'size': '40'
            }
        )
    )
    langkah = forms.CharField(
        label="langkah",
        max_length=1500,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'required': True,
                'style': 'margin-bottom: 4%; margin-top: 2%;',
                'size': '40'
            }
        )
    )
