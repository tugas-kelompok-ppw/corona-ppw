from django.shortcuts import render, HttpResponseRedirect
from .models import resep
from .forms import buatresep


def index(request):
    return render(request, "health.html")


def inputform(request):
    kumpulanresep = resep.objects.all()
    response = {
        'reseps': kumpulanresep
    }
    return render(request, "isidong.html", response)
# Create your views here.


def mintaresep(request):
    form = buatresep()
    if request.method == 'POST':
        form = buatresep(request.POST)

        resep.objects.create(
            nama=request.POST['nama'],
            bahan=request.POST['bahan'],
            alat=request.POST['alat'],
            langkah=request.POST['langkah'],

        )

        return HttpResponseRedirect('/health/isiform')

    response = {
        'form': form
    }
    return render(request, "request.html", response)
