from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps

from .models import Events
from .views import events, preform, postform
from .forms import EventsForm
from .apps import FeatureAayaConfig

# Create your tests here.
class TestingApp(TestCase):
    def test_app(self):
        self.assertEqual(FeatureAayaConfig.name, 'feature_aaya')
        self.assertEqual(apps.get_app_config('feature_aaya').name, 'feature_aaya')

class TestingModel(TestCase):
    def setUp(self):
        self.events = Events.objects.create(
            event_name = "perak",
            event_date = "april",
            event_desc = "fun",
            contact = "0812 aaya"
        )

    def test_instance(self):
        self.assertEqual(Events.objects.count(), 1)

class TestingForm(TestCase):
    def test_form_valid(self):
        events_form = EventsForm(
            data={
                "event_name" : "perak",
                "event_date" : "april",
                "event_desc" : "fun",
                "contact" : "0812 aaya"
            }
        )
        self.assertTrue(events_form.is_valid())

    def test_form_invalid(self):
        events_form = EventsForm(
            data={}
        )
        self.assertFalse(events_form.is_valid())

class TestingViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.Events = Events.objects.create(
            event_name="perak",
            event_date="april",
            event_desc="fun",
            contact="0812 aaya"
        )
        self.events = reverse("events:events")
        self.preform = reverse("events:preform")
        self.postform = reverse("events:postform")

    def test_events_get(self):
        response = self.client.get(self.events)
        self.assertEqual(response.status_code, 200)

    def test_preform_get(self):
        response = self.client.get(self.preform)
        self.assertEqual(response.status_code, 200)

    def test_postform_get(self):
        response = self.client.get(self.postform)
        self.assertEqual(response.status_code, 200)

    def test_preform_post(self):
        response = self.client.post(
            self.preform,
            {
                "event_name" : "perak",
                "event_date" : "april",
                "event_desc" : "fun",
                "contact" : "0812 aaya"
            },
            follow=True
            )
        self.assertEqual(response.status_code, 200)

    def test_preform_post_invalid(self):
        response = self.client.post(
            self.preform,
            {
                "event_name" : "",
                "event_date" : "",
                "event_desc" : "",
                "contact" : ""
            },
            follow=True
        )
        self.assertTemplateUsed(response, 'postform.html')

class TestingUrls(TestCase):
    def setUp(self):
        self.Events = Events.objects.create(
            event_name="perak",
            event_date="april",
            event_desc="fun",
            contact="0812 aaya"
        )
        self.events = reverse("events:events")
        self.preform = reverse("events:preform")
        self.postform = reverse("events:postform")

    def test_events_function(self):
        found = resolve(self.events)
        self.assertEqual(found.func, events)
    
    def test_preform_function(self):
        found = resolve(self.preform)
        self.assertEqual(found.func, preform)
    
    def test_postform_function(self):
        found = resolve(self.postform)
        self.assertEqual(found.func, postform)
