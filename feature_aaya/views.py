# Create your views here.
from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import Events
from .forms import EventsForm
# Create your views here.

def events(request):
    return render(request, 'events.html')

# fungsi untuk hasil form (details)
def postform(request):
    events = Events.objects.all()
    response = {
        'events' : events
    }
    return render(request, 'postform.html', response)

# fungsi untuk form
def preform(request):
    regist = EventsForm()
    if request.method == 'POST':
        regist = EventsForm(request.POST)

        Events.objects.create(
            event_name = request.POST['event_name'],
            event_date = request.POST['event_date'],
            event_desc = request.POST['event_desc'],
            contact = request.POST['contact'],
        )

        return HttpResponseRedirect('/events/postform')

    response = {
        'regist' : regist
    }

    return render(request, 'preform.html', response)