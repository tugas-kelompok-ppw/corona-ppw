from django.urls import path
from . import views

app_name = 'feature_aaya'
urlpatterns = [
    path('', views.events, name='events'),
    path('preform/', views.preform, name='preform'),
    path('postform/', views.postform, name='postform')
]