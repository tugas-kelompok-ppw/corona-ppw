from django import forms
from .models import Events

class EventsForm(forms.Form):
    event_name = forms.CharField(
        widget = forms.TextInput(
            attrs={
                'class':'input-height',
            }
        )
    )
    event_date = forms.CharField(
       widget = forms.TextInput(
            attrs={
                'class':'input-height',
            }
        )
    )
    event_desc = forms.CharField(
        widget = forms.Textarea(
            attrs={
                'class':'input-width',
            }
        )
    )
    contact = forms.CharField(
        widget = forms.TextInput(
            attrs={
                'class':'input-height',
            }
        )
    )
    
    