# Now What?

## Anggota Kelompok

- Mirsa Salsabila / 1906350875
- Aaya Faizah / 1906353800
- Nathanael Pardosi / 1906353826
- Azzahra Banowati Rahmah / 1906353782
- Dionisius Baskoro Samudra / 1906350774

## Tentang Now What?

### Aplikasi ini merupakan media atau portal untuk masyarakat, agar di masa pandemi ini masyarakat dapat dengan mudah mencari hiburan baik berupa musik, film, online events, bahkan aktivitas baru yang dapat mereka coba di rumah. Aplikasi ini diharapkan dapat membantu masyarakat untuk menghilangkan kejenuhan dan lebih merawat diri, khususnya kesehatan mental. Untuk itu, aplikasi ini juga menyajikan fitur yang sekiranya dapat memberi masukkan bagi masyarakat bagaimana untuk menjaga kesehatan mental mereka.

## Daftar fitur

- Movies Recommendation (Azzahra Banowati Rahmah)
- Music Recommendation (Dionisius Baskoro Samudra)
- Health Recommendation (Nathanael Pardosi)
- Events Recommendation (Aaya Faizah)
- Activities Recommendation (Mirsa Salsabila)

[![pipeline status](https://gitlab.com/tugas-kelompok-ppw/corona-ppw/badges/master/pipeline.svg)](https://gitlab.com/tugas-kelompok-ppw/corona-ppw/-/commits/master)
