from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import Music_Form
from .models import Music

def index(request):
	music = Music.objects.all()
	context = {
		'music':music
	}

	return render(request, "home.html", context)

def share(request):
	music_form = Music_Form

	if (request.method == 'POST'):
		Music.objects.create(
			title = request.POST.get('title'),
			artist = request.POST.get('artist'),
			link = request.POST.get('link'),
			images = request.POST.get('images')
		)
		return HttpResponseRedirect('/music')

	context = {
		'music_form':music_form
	}

	return render(request, 'share.html', context)
