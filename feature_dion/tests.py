from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps

from .views import index, share
from .models import Music
from .apps import FeatureDionConfig

# Create your tests here.
class TestingApp(TestCase):
    def test_app_is_exist(self):
        self.assertEqual(FeatureDionConfig.name, 'feature_dion')
        self.assertEqual(apps.get_app_config('feature_dion').name, 'feature_dion')

class TestRoutingIndex(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/music/')
        self.assertEqual(response.status_code, 200)
    
    def test_used_index_template(self):
        response = Client().get('/music/')
        self.assertTemplateUsed(response, 'home.html')

class TestRoutingShare(TestCase):
    def test_share_url_is_exist(self):
        response = Client().get('/music/share/')
        self.assertEqual(response.status_code, 200)

    def test_used_share_template(self):
        response = Client().get('/music/share/')
        self.assertTemplateUsed(response, 'share.html')

    def test_share_func(self):
        found = resolve('/music/share/')
        self.assertEqual(found.func, share)

class TestShare(TestCase):
    def setUp(self):
        shareMusic = Music(title = "AAA")
        self.assertEqual(Music.objects.all().count(), 0)

    def test_share_music(self):
        response = Client().post('/music/share/',{'title' : 'AAAA', 'artist' : 'BBBB', 'link' : 'CCCC', 'images' : 'DDDD'})
        self.assertEqual(response.status_code, 302)

    def test_share_model(self):
        music = Music.objects.create(title='Maps', artist='Maroon 5', link='https://abcd.com', images='https://abcd.com')
        self.assertEqual(music.__str__(), 'Maps')