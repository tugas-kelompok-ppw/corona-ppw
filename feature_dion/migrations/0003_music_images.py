# Generated by Django 3.1.1 on 2020-11-14 22:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feature_dion', '0002_auto_20201115_0443'),
    ]

    operations = [
        migrations.AddField(
            model_name='music',
            name='images',
            field=models.CharField(default='http://www.klrc.com/themes/klrcresponsive/images/media-player/album-art-default.png', max_length=1000),
        ),
    ]
