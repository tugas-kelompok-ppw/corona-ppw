from django.urls import path
from . import views

app_name = 'feature_dion'
urlpatterns = [
    path('', views.index, name="music"),
    path('share/', views.share, name="share")
]