from django import forms
from .models import Music
# from django.forms import ModelForm

class Music_Form(forms.Form):
    title = forms.CharField(
        max_length = 100,
        widget = forms.TextInput(
            attrs = {
                'class':'input-width',
                'placeholder':'Telenovia'
            }
        )
    )

    artist = forms.CharField(
        max_length = 100,
        widget = forms.TextInput(
            attrs = {
                'class':'input-width',
                'placeholder':'Reality Club'
            }
        )
    )

    link = forms.CharField(
        max_length = 1000,
        widget = forms.TextInput(
            attrs = {
                'class':'input-width',
                'placeholder':'https://open.spotify.com/'
            }
        )
    )

    images = forms.CharField(
        max_length = 1000,
        initial = 'http://www.klrc.com/themes/klrcresponsive/images/media-player/album-art-default.png',
        widget = forms.TextInput(
            attrs = {
                'class':'input-width',
                'placeholder':'https://google.png/',
            }
        )
    )