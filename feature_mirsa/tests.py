from django.test import TestCase, Client 
from django.urls import resolve, reverse
from django.apps import apps


from .models import Story
from .views import activities, story, tambah_story
from .forms import StoryForm
from .apps import FeatureMirsaConfig

# Create your tests here.

class TestApp(TestCase):
    def test_apps_is_exist(self):
        self.assertEquals(FeatureMirsaConfig.name, 'feature_mirsa')
        self.assertEquals(apps.get_app_config('feature_mirsa').name, 'feature_mirsa')


class TestActivities(TestCase):
    def test_activities_url_is_exist(self):
        response = Client().get('/activities/')
        self.assertEqual(response.status_code, 200)
    
    def test_activities_index_func(self):
        found = resolve('/activities/')
        self.assertEqual(found.func,activities)

    def test_activities_using_template(self):
        response = Client().get('/activities/')
        self.assertTemplateUsed(response, 'activities.html')

class TestTambahStory(TestCase):
    def test_TambahStory_url_is_exist(self):
        response = Client().get('/activities/tambah/')
        self.assertEqual(response.status_code, 200)
    
    def test_activities_index_func(self):
        found = resolve('/activities/tambah/')
        self.assertEqual(found.func,tambah_story)

    def test_activities_using_template(self):
        response = Client().get('/activities/tambah/')
        self.assertTemplateUsed(response, 'story.html')

class TestListStory(TestCase):
    def test_ListStory_url_is_exist(self):
        response = Client().get('/activities/list_story/')
        self.assertEqual(response.status_code, 200)
    
    def test_activities_index_func(self):
        found = resolve('/activities/list_story/')
        self.assertEqual(found.func,story)

    def test_activities_using_template(self):
        response = Client().get('/activities/list_story/')
        self.assertTemplateUsed(response, 'list_story.html')

class TestModel(TestCase):
    def setUp(self):
        self.Story = Story.objects.create(
            name="aku", experience="bagus"
        )

    def test_instance_created(self):
        self.assertEqual(Story.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.Story), "aku")



class TestForm(TestCase):
    def test_form_is_valid(self):
        form = StoryForm(
            data={'name':'aku', 'experience':'coba'}
        )
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = StoryForm(data={})
        self.assertFalse(form.is_valid())