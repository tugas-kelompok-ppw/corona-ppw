from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import StoryForm
from .models import Story

# Create your views here.
def activities(request):
    return render(request, 'activities.html')


def story(request):
    if request.method == "POST":
        Story.objects.get(id=request.POST['id']).delete()
        return redirect('/activities/list_story/')
    cerita = Story.objects.all()
    context = {'list_story':cerita}
    return render(request, 'list_story.html', context)
    

def tambah_story(request):
    submit = False
    if request.method == 'POST':
        form = StoryForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/activities/tambah/?submit=True')
    else:
        form = StoryForm
        if 'submit' in request.GET:
            submit = True
    context = {'form': form, 'submit': submit}
    return render(request, 'story.html', context)