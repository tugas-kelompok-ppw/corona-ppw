from django.forms import ModelForm
from .models import Story

class StoryForm(ModelForm):
	required_css_class = 'required'
	class Meta:
		model = Story
		fields = '__all__'