from django.urls import path
from . import views

app_name = 'feature_mirsa'

urlpatterns = [
    path('', views.activities, name='activities'),
    path('tambah/', views.tambah_story, name='tambah_story'),
    path('list_story/', views.story, name='story'),
    # dilanjutkan ...
]